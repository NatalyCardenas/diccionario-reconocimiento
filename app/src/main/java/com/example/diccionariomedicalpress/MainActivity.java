package com.example.diccionariomedicalpress;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    String url;
    private TextView mostrarDef;
    private EditText ingresarPalabra;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mostrarDef = findViewById(R.id.mostrarDef);
        ingresarPalabra = findViewById(R.id.ingresarPalabra);
    }
    private String dictionaryEntries() {
        final String language = "en-gb";
        final String word = ingresarPalabra.getText().toString();//  right now it will only shoy meaninf of this word
        final String fields = "definitions";
        final String strictMatch = "false";
        final String word_id = word.toLowerCase();
        return "https://od-api.oxforddictionaries.com:443/api/v2/entries/" + language + "/" + word_id + "?" + "fields=" + fields + "&strictMatch=" + strictMatch;
    }
    public void sendRequestOnClick(View v){
        DictionaryRequest dR= new DictionaryRequest(this, mostrarDef);
        url= dictionaryEntries();
        dR.execute(url);


    }
}